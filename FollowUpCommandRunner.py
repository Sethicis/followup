#!/usr/bin/env python3.7

import asyncio
import iterm2
from Components import get_current_session, async_get_follow_up_variable, async_update_follow_up_variable, convert_command_list_to_str
from typing import Dict, List

# This script was created with the "basic" environment which does not support adding dependencies
# with pip.


def get_dependent_starter() -> str:
    return '[[ $? == 0 ]] && '


def is_dependent_command_sequence(commands: str) -> bool:
    command_pieces = commands.split(' ')
    return command_pieces and command_pieces[0] == '&&'


def is_independent_sequence(commands: str) -> bool:
    command_pieces = commands.split(' ')
    return command_pieces and command_pieces[0] == ';'


def is_ambiguous_sequence(commands: str) -> bool:
    return not is_dependent_command_sequence(commands) and not is_independent_sequence(commands)


def prepare_commands(pending_commands: List[str], commands: str):
    if not pending_commands:
        if (is_dependent_command_sequence(commands) or is_ambiguous_sequence(commands)):    
            if is_dependent_command_sequence(commands):
                pending_commands.extend([commands.replace('&&', get_dependent_starter(), 1)])
            else:
                pending_commands.extend([get_dependent_starter(), commands])
        else:
            pending_commands.extend([commands.lstrip(' ;')])
    else:
        if is_ambiguous_sequence(commands):
            pending_commands.extend(['&&', commands])
        else:
            pending_commands.extend([commands])


async def main(connection: iterm2.connection.Connection):
    alert = iterm2.TextInputAlert(title="Follow up command(s)", subtitle="Enter additional command(s) to run.", placeholder="command(s)", defaultValue="")
    try:
        commands: str = await alert.async_run(connection)
        if not commands.strip():
            return
        pending_commands = await async_get_follow_up_variable(connection)

        prepare_commands(pending_commands, commands)
        await async_update_follow_up_variable(connection, pending_commands)
    except Exception as e:
        print("WARNING - Could not get user follow up commands.")
        print(e)

# This instructs the script to run the "main" coroutine and to keep running even after it returns.
iterm2.run_until_complete(main)
