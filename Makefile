target_path := ~/Library/Application\ Support/iTerm2/Scripts/
script_path := $(target_path)FollowUp/
autoload_path := $(target_path)AutoLaunch/

install:
	mkdir -p $(script_path)
	mkdir -p $(autoload_path)
	cp -R Components $(script_path)
	cp FollowUpCommandDaemon.py $(script_path)
	cp FollowUpCommandRunner.py $(script_path)
	cp FollowUpCommandDisplay.py $(script_path)
	ln -sf $(script_path) $(autoload_path)FollowUpCommandDaemon.py
