#!/usr/bin/env python3.7

import iterm2
from typing import List
from Components import async_get_follow_up_variable, convert_command_list_to_str

async def main(connection: iterm2.connection.Connection):
    pending_commands: List[str] = await async_get_follow_up_variable(connection)
    if not pending_commands:
        commands = 'None'
    else:
        commands = convert_command_list_to_str(pending_commands)
    alert = iterm2.Alert("List of pending commands for session", commands)
    await alert.async_run(connection)

# This instructs the script to run the "main" coroutine and to keep running even after it returns.
iterm2.run_until_complete(main)
